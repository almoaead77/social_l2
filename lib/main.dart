import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/STM/Midllewares/authMiddleware.dart';
import 'package:social/Logic/STM/Services/storageService.dart';
import 'package:social/UI/Screens/user/home.dart';
import 'package:social/utils/routes.dart';
import 'Logic/STM/Bindings/storageBinding.dart';
import 'UI/Screens/Auth/login.dart';
import 'UI/Screens/Auth/register.dart';

void main() async{
  await GetStorage.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder: (context,orientation,deviceType) {
        return GetMaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.indigo,
          ),
          darkTheme: ThemeData(
            brightness: Brightness.dark,
            primaryColorDark: Colors.indigo,
            accentColor: Colors.white
          ),
          themeMode: ThemeMode.light,
          getPages: [
            GetPage(
              name: LoginScreen.name,
              page: ()=> LoginScreen(),
              middlewares: [
                AuthMiddleware()
              ],
            ),
            GetPage(name: Routes.register, page: ()=> RegisterScreen()),
            GetPage(name: Routes.home, page: () => HomeScreen())
          ],
          initialRoute: Routes.login,
          initialBinding: StorageBending()
        );
      },
    );
  }
}
