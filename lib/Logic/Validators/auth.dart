import 'package:get/get.dart';

class AuthValidator{

  static String? password(String? value){
    if( value == null || value.isEmpty)
      return 'write your password';
    else if(value.length < 4)
      return 'password should be at least 4 characters';
  }

  static String? email (String? value){
    if( value == null || ! value.isEmail )
      return 'error invalid email';
  }

  static String? name (String? value){
    if( value == null ||  value.length < 2 )
      return 'error enter your name';
  }

  static String? phone (String? value){
    if( value == null ||  value.isEmpty)
      return 'error enter your phone';
    if( ! value.isPhoneNumber)
      return 'error enter correct phone number';
  }



}