import 'package:get/get.dart';
import '../Services/storageService.dart';

class StorageBending extends Bindings{
  @override
  void dependencies() {
    Get.put<StorageService>(StorageService(),permanent: true);
    //Get.lazyPut(() => StorageService(),);
  }
}