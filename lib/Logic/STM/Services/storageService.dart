import 'package:get/get.dart';
//import 'package:get_storage/get_storage.dart';


class StorageService extends GetxService{

  //late final GetStorage _storage ;

  bool _islogin = false;

  @override
  void onInit() {
    //_storage = GetStorage();
  }

  bool get isAuth {
    return _islogin;
  }

  Future<void> login() async{
    _islogin = true;
    //await _storage.write('token', 'value');
  }

  Future<void> logout() async{
    //await _storage.remove('token');
    _islogin = false;
  }

}