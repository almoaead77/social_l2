import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class ProfileHeader extends StatelessWidget {

  String? _path ;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(2.5.w),
      child: Column(
        children: [
          StatefulBuilder(
            builder: (context,setState) {
              return GestureDetector(
                onTap: () async{
                  var imagePicker = ImagePicker();
                  var xfile = await imagePicker.pickImage(
                      source: ImageSource.gallery
                  );
                  if( xfile != null){
                    setState((){
                      _path = xfile!.path;
                    });
                  }
                  else {
                    setState(() {
                      _path = null;
                    });
                  }
                },
                child: CircleAvatar(
                  foregroundImage: _path == null ?
                  null:
                  FileImage(File(_path!)),
                  radius: 25.w,
                ),
              );
            }
          ),
          SizedBox(
            height: 5.w,
          ),
          Text('User Name',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.sp
              ),
          )
        ],
      )
    );
  }
}
