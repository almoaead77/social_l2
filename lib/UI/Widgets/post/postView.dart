import 'package:flutter/material.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:sizer/sizer.dart';

class PostView extends StatelessWidget {

  final bool hasImage;
  final bool hasPostImage;


  PostView({this.hasImage = false, this.hasPostImage = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(2.5.w),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: _user,
              title: Text('name'),
              subtitle: Text(DateTime.now().toString()),
            ),
            Divider(
              height: 0,
            ),
            Padding(
              padding: EdgeInsets.all(1.w),
              child: Text('''this is post this is post this is post
this is post this is post
this is post this is post
this is post
this is post          
              '''),
            ),
            Visibility(
                visible: hasPostImage,
                child: Center(
                  child: FancyShimmerImage(
                    imageUrl: 'https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359553_1280.png',
                    errorWidget: Center(
                      child: Icon(Icons.image_not_supported_outlined,size: 50.sp,),
                    ),
                  ),
                ),
            ),
            Divider(),
            Reactions()
          ],
        ),
      ),
    );
  }

  Widget get _user{
    if(hasImage)
      return CircleAvatar(
        foregroundImage: NetworkImage('https://cdn.pixabay.com/photo/2020/07/01/12/58/icon-5359553_1280.png'),
      );
    return CircleAvatar(
      child: Icon(Icons.person),
    );
  }
}

class Reactions extends StatelessWidget {
  const Reactions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        IconButton(
            onPressed: (){},
            icon: Icon(Icons.thumb_up_alt_outlined)
        ),
        IconButton(
            onPressed: (){},
            icon: Icon(Icons.thumb_down_alt_outlined)
        ),
        IconButton(
            onPressed: (){},
            icon: Icon(Icons.comment)
        ),
      ],
    );
  }
}
