import 'package:flutter/material.dart';


class CustomField extends StatefulWidget {

  final FormFieldValidator<String>? validator;
  final TextEditingController? controller;
  final String? hint;
  final Widget? icon;
  final bool isPass;
  final TextInputType? keyboardType;
  bool _hide = true;

  CustomField({
    this.controller,
    this.hint,
    this.icon,
    this.keyboardType,
    this.isPass = false,
    this.validator
  });

  @override
  State<CustomField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<CustomField> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(size.width*0.025),
      child: TextFormField(
        validator: widget.validator,
        controller: widget.controller,
        decoration: InputDecoration(
            hintText: widget.hint,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(size.width*0.025),
                borderSide: BorderSide.none
            ),
            filled: true,
            fillColor: Colors.grey[300],
            prefixIcon: widget.icon,
            suffixIcon: widget.isPass ?
            IconButton(
                onPressed: (){
                  setState((){
                    widget._hide = !widget._hide;
                  });
                },
                icon: widget._hide ?
                Icon(Icons.visibility_off):
                Icon(Icons.visibility)
            ) : null
        ),
        obscureText: widget.isPass && widget._hide,
        keyboardType: widget.keyboardType,
      ),
    );
  }
}
