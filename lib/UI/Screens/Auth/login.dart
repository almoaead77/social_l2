import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/Validators/auth.dart';
import 'package:social/UI/Widgets/myTextFiels.dart';
import 'package:social/UI/Widgets/pagesImages.dart';
import 'package:social/utils/routes.dart';

import '../../../Logic/STM/Services/storageService.dart';
import '../../../utils/images.dart';

class LoginScreen extends StatelessWidget {

  static final name = '/login';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('login'),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: SvgPicture.asset(
              Images.loginBackground,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  PageImage(Images.loginImage),
                  CustomField(
                    hint: 'email',
                    keyboardType: TextInputType.emailAddress,
                    validator: AuthValidator.email ,
                    icon: Icon(Icons.email_outlined),
                  ),
                  CustomField(
                    hint: 'password',
                    validator: AuthValidator.password,
                    isPass: true,
                    icon: Icon(Icons.lock_outline),
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        if(!_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text('data error')));
                        }
                        else{
                          var storage = Get.find<StorageService>();
                          await storage.login();
                          Get.offAllNamed(Routes.home);
                        }
                      },
                      child: Text('login')
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  InkWell(
                    child: Text(
                        'register if you don\'t have an account',
                      style: TextStyle(
                        fontSize: 12.sp
                      ),
                    ),
                    onTap: ()=>Get.offNamed(Routes.register),
                  ),

                  SizedBox(
                    height: MediaQuery.of(context).viewInsets.bottom,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
