import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:social/Logic/Validators/auth.dart';
import 'package:social/UI/Widgets/myTextFiels.dart';
import 'package:social/UI/Widgets/pagesImages.dart';
import 'package:social/utils/routes.dart';

import '../../../utils/images.dart';

class RegisterScreen extends StatelessWidget {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _passController = TextEditingController();
  final TextEditingController _cpassController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text('register'),
      ),
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            child: SvgPicture.asset(Images.registerBackground,
              fit: BoxFit.fill,
            ),
          ),
          SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  PageImage(Images.registerImage),
                  CustomField(
                    hint: 'name',
                    keyboardType: TextInputType.name,
                    validator: AuthValidator.name,
                    icon: Icon(Icons.person_outline),
                  ),
                  CustomField(
                    hint: 'email',
                    keyboardType: TextInputType.emailAddress,
                    validator: AuthValidator.email,
                    icon: Icon(Icons.email_outlined),
                  ),
                  CustomField(
                    keyboardType: TextInputType.phone,
                    hint: 'phone',
                    validator: AuthValidator.phone,
                    icon: Icon(Icons.phone_outlined),
                  ),
                  CustomField(
                    controller: _passController,
                    hint: 'password',
                    validator: AuthValidator.password,
                    isPass: true,
                    icon: Icon(Icons.lock_outline),
                  ),
                  CustomField(
                    controller: _cpassController,
                    hint: 'confirm password',
                    validator: (value){
                      var error = AuthValidator.password(value);
                      if(error != null)
                        return error;

                      if(_cpassController.value != _passController.value)
                        return 'error confirm password is wrong';

                    },
                    isPass: true,
                    icon: Icon(Icons.lock_outline),
                  ),
                  ElevatedButton(
                      onPressed: (){
                        if(!_formKey.currentState!.validate()) {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(content: Text('data error')));
                        }
                        else{
                          Get.offAllNamed(Routes.home);
                        }
                      },
                      child: Text('register'),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  InkWell(
                    child: Text('login if you have an account',
                    style: TextStyle(
                      fontSize: 12.sp
                    ),
                    ),
                    onTap: ()=>Get.offNamed(Routes.login),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).viewInsets.bottom + 5.h,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
