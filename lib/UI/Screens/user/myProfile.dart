import 'package:flutter/material.dart';
import 'package:social/UI/Widgets/profile/profileHeader.dart';

import '../../Widgets/post/postView.dart';

class MyProfile extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ProfileHeader(),
        ListView.builder(
            physics: PageScrollPhysics(),
            shrinkWrap: true,
            itemCount: 10,
            itemBuilder: (context,i) => PostView(
              hasImage: i.isEven,
              hasPostImage: i < 3,
            )
        )
      ],
    );
  }
}
